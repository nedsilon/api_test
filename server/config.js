// This can be changed in an .env file
const config = {
    secret: process.env.SECRET
}

module.exports = {
    config
}