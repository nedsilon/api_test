const express = require('express')
const mongodb = require('mongodb')

const router = express.Router()


// Get Posts
router.get('/', async (req, res) => {
    const data = await loadMainCollection()
    res.send(await data.find({}).toArray())
})

async function loadMainCollection() {
    const client = await mongodb.MongoClient.connect
    (process.env.MONGO_API, {useNewUrlParser: true})
    return client.db(process.env.MONGO_DB_NAME).collection('mainData')
}

module.exports = router

